﻿using Eve.CSharp.Controller;
using Eve.CSharp.Esi.Model.Universe;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Eve.CSharp.Esi.Controller
{
    internal class UniverseController : EsiController
    {
        public UniverseController(EsiEngine engine) : base(engine) { }

        internal async Task<List<int>> GetRegionIds()
        {
            HttpClient client = new HttpClient();
            string url = Engine.BaseURL + $"universe/regions/?datasource={Engine.DataSource}";
            Uri uri = new Uri(url);
            List<int> results = new List<int>();

            try
            {
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                    results = JsonConvert.DeserializeObject<List<int>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return results;
        }

        internal async Task<Region> GetRegionDetails(int regionId)
        {
            HttpClient client = new HttpClient();
            string url = Engine.BaseURL + $"universe/regions/{regionId}/?datasource={Engine.DataSource}";
            Uri uri = new Uri(url);
            Region result = new Region();

            try
            {
                HttpResponseMessage response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                    result = JsonConvert.DeserializeObject<Region>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        internal async Task<List<Region>> GetRegionDetails(List<int> regionIds)
        {
            using (HttpClient client = new HttpClient())
            {
                List<Region> results = new List<Region>();

                foreach (int regionId in regionIds)
                {
                    string url = Engine.BaseURL + $"universe/regions/{regionId}/?datasource={Engine.DataSource}";
                    Uri uri = new Uri(url);

                    try
                    {
                        HttpResponseMessage response = await client.GetAsync(uri);
                        if (response.IsSuccessStatusCode)
                            results.Add(JsonConvert.DeserializeObject<Region>(await response.Content.ReadAsStringAsync()));

                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

                return results;
            }
        }

        internal async Task<Constellation> GetConstellationDetails(int constellationId)
        {
            using (HttpClient client = new HttpClient())
            {
                Constellation result = new Constellation();

                string url = Engine.BaseURL + $"universe/constellations/{constellationId}/?datasource={Engine.DataSource}";
                Uri uri = new Uri(url);

                try
                {
                    HttpResponseMessage response = await client.GetAsync(uri);
                    if (response.IsSuccessStatusCode)
                        result = JsonConvert.DeserializeObject<Constellation>(await response.Content.ReadAsStringAsync());
                }
                catch (Exception)
                {

                    throw;
                }

                return result;
            }
        }

        internal async Task<List<Constellation>> GetConstellationDetails(List<int> constellationIds)
        {
            using (HttpClient client = new HttpClient())
            {
                List<Constellation> results = new List<Constellation>();

                foreach (int constellationId in constellationIds)
                {
                    string url = Engine.BaseURL + $"universe/constellations/{constellationId}/?datasource={Engine.DataSource}";
                    Uri uri = new Uri(url);

                    try
                    {
                        HttpResponseMessage response = await client.GetAsync(uri);
                        if (response.IsSuccessStatusCode)
                            results.Add(JsonConvert.DeserializeObject<Constellation>(await response.Content.ReadAsStringAsync()));
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

                return results;
            }
        }

        internal async Task<StarSystem> GetSystemDetails(int systemId)
        {
            using (HttpClient client = new HttpClient())
            {
                StarSystem result = new StarSystem();
                string url = Engine.BaseURL + $"universe/systems/{systemId}/?datasource={Engine.DataSource}";
                Uri uri = new Uri(url);

                try
                {
                    HttpResponseMessage response = await client.GetAsync(uri);
                    if (response.IsSuccessStatusCode)
                        result = JsonConvert.DeserializeObject<StarSystem>(await response.Content.ReadAsStringAsync());
                }
                catch (Exception)
                {

                    throw;
                }

                return result;
            }
        }

        internal async Task<List<StarSystem>> GetSystemDetails(List<int> systemIds)
        {
            using (HttpClient client = new HttpClient())
            {
                List<StarSystem> results = new List<StarSystem>();

                foreach (int systemId in systemIds)
                {
                    string url = Engine.BaseURL + $"universe/constellations/{systemId}/?datasource={Engine.DataSource}";
                    Uri uri = new Uri(url);

                    try
                    {
                        HttpResponseMessage response = await client.GetAsync(uri);
                        if (response.IsSuccessStatusCode)
                            results.Add(JsonConvert.DeserializeObject<StarSystem>(await response.Content.ReadAsStringAsync()));
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

                return results;
            }
        }
    }
}
