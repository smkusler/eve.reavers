﻿using Eve.CSharp.Esi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Controller
{
    public abstract class EsiController
    {
        public EsiEngine Engine { get; set; }

        public EsiController(EsiEngine engine)
        {
            Engine = engine;
        }
    }
}
