﻿using Eve.CSharp.Controller;
using Eve.CSharp.Model.Market;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Esi
{
    internal class MarketController : EsiController
    {
        public MarketController(EsiEngine engine) : base(engine) { }

        public async Task<List<Price>> GetPrices()
        {
            using (HttpClient client = new HttpClient())
            {
                string url = base.Engine.BaseURL + $"markets/prices/?datasource={Engine.DataSource}";
                Uri uri = new Uri(url);
                List<Price> results = new List<Price>();

                try
                {
                    HttpResponseMessage response = await client.GetAsync(uri);
                    if (response.IsSuccessStatusCode)
                        results = JsonConvert.DeserializeObject<List<Price>>(await response.Content.ReadAsStringAsync());
                }
                catch (Exception)
                {

                    throw;
                }

                return results;
            }
        }

        public async Task<List<int>> GetItemGroups()
        {
            using (HttpClient client = new HttpClient())
            {
                string url = base.Engine.BaseURL + $"markets/groups/?datasource={Engine.DataSource}";
                Uri uri = new Uri(url);
                List<int> results = new List<int>();

                try
                {
                    HttpResponseMessage response = await client.GetAsync(uri);
                    if (response.IsSuccessStatusCode)
                        results = JsonConvert.DeserializeObject<List<int>>(await response.Content.ReadAsStringAsync());
                }
                catch (Exception)
                {

                    throw;
                }

                return results;
            }
        }

        public async Task<ItemGroup> GetItemGroupDetails(int groupId)
        {
            using (HttpClient client = new HttpClient())
            {
                string url = base.Engine.BaseURL + $"markets/groups/{groupId}/?datasource={Engine.DataSource}";
                Uri uri = new Uri(url);
                ItemGroup results = new ItemGroup();

                try
                {
                    HttpResponseMessage response = await client.GetAsync(uri);
                    if (response.IsSuccessStatusCode)
                        results = JsonConvert.DeserializeObject<ItemGroup>(await response.Content.ReadAsStringAsync());
                }
                catch (Exception)
                {

                    throw;
                }

                return results;
            }
        }

        /// <summary>
        ///     Gets all region orders unless filtered by typeId or a page range.
        /// </summary>
        /// <param name="regionId">Region id for area to retrieve orders</param>
        /// <param name="typeId">Filter to orders only of this type.  0 is all types.</param>
        /// <param name="startingPage">Starting page to use.  0 is all pages.</param>
        /// <param name="endingPage">Ending page to use.  0 is all pages.</param>
        /// <returns></returns>
        public async Task<List<Order>> GetRegionOrders(int regionId, int typeId = 0, int startingPage = 0, int endingPage = 0)
        {
            if (startingPage == 0 || endingPage == 0)
            {
                if (typeId > 0)
                    return await GetAllRegionOrdersByType(regionId, typeId);

                return await GetAllRegionOrders(regionId);
            }
            else
            {
                if (typeId > 0)
                    return await GetRegionOrdersByTypeByPageRange(regionId, typeId, startingPage, endingPage);

                return await GetRegionOrdersByPageRange(regionId, startingPage, endingPage);
            }
        }

        private async Task<List<Order>> GetAllRegionOrders(int regionId)
        {
            using (HttpClient client = new HttpClient())
            {
                List<Order> results = new List<Order>();
                bool reachedEnd = false;
                int pageNumber = 1;

                do
                {
                    string url = base.Engine.BaseURL + $"markets/{regionId}/orders/?datasource={Engine.DataSource}&order_type=all&page={pageNumber}";
                    Uri uri = new Uri(url);

                    try
                    {
                        HttpResponseMessage response = await client.GetAsync(uri);
                        if (response.IsSuccessStatusCode)
                        {
                            List<Order> orders = JsonConvert.DeserializeObject<List<Order>>(await response.Content.ReadAsStringAsync());

                            if (orders == null || orders.Count == 0)
                                reachedEnd = true;
                            else
                            {
                                results.AddRange(orders);
                            }
                        }
                        else if (response.ReasonPhrase == "Not Found")
                        {
                            reachedEnd = true;
                        }

                        pageNumber++;
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                } while (!reachedEnd);

                return results;
            }
        }

        private async Task<List<Order>> GetAllRegionOrdersByType(int regionId, int typeId)
        {
            using (HttpClient client = new HttpClient())
            {
                List<Order> results = new List<Order>();
                bool reachedEnd = false;
                int pageNumber = 1;

                do
                {
                    string url = base.Engine.BaseURL + $"markets/{regionId}/orders/?datasource={Engine.DataSource}&order_type=all&page={pageNumber}&type_id={typeId}";
                    Uri uri = new Uri(url);

                    try
                    {
                        HttpResponseMessage response = await client.GetAsync(uri);
                        if (response.IsSuccessStatusCode)
                        {
                            List<Order> orders = JsonConvert.DeserializeObject<List<Order>>(await response.Content.ReadAsStringAsync());

                            if (orders == null || orders.Count == 0)
                                reachedEnd = true;
                            else
                            {
                                results.AddRange(orders);
                            }

                            pageNumber++;
                        }
                        else if (response.ReasonPhrase == "Not Found")
                        {
                            reachedEnd = true;
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                } while (!reachedEnd);

                return results;
            }
        }

        private async Task<List<Order>> GetRegionOrdersByPageRange(int regionId, int startingPage, int endingPage)
        {
            using (HttpClient client = new HttpClient())
            {
                List<Order> results = new List<Order>();
                bool reachedEnd = false;
                int pageNumber = startingPage;

                do
                {
                    string url = base.Engine.BaseURL + $"markets/{regionId}/orders/?datasource={Engine.DataSource}&order_type=all&page={pageNumber}";
                    Uri uri = new Uri(url);

                    try
                    {
                        HttpResponseMessage response = await client.GetAsync(uri);
                        if (response.IsSuccessStatusCode)
                        {
                            List<Order> orders = JsonConvert.DeserializeObject<List<Order>>(await response.Content.ReadAsStringAsync());

                            if (orders == null || orders.Count == 0)
                                reachedEnd = true;
                            else if (pageNumber == endingPage)
                            {
                                reachedEnd = true;
                                results.AddRange(orders);
                            }
                            else
                            {
                                results.AddRange(orders);
                            }

                            pageNumber++;
                        }
                        else if (response.ReasonPhrase == "Not Found")
                        {
                            reachedEnd = true;
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                } while (!reachedEnd);

                return results;
            }
        }

        private async Task<List<Order>> GetRegionOrdersByTypeByPageRange(int regionId, int typeId, int startingPage, int endingPage)
        {
            using (HttpClient client = new HttpClient())
            {
                List<Order> results = new List<Order>();
                bool reachedEnd = false;
                int pageNumber = startingPage;

                do
                {
                    string url = base.Engine.BaseURL + $"markets/groups/{regionId}/orders/?datasource={Engine.DataSource}&order_type={typeId}&page={pageNumber}";
                    Uri uri = new Uri(url);

                    try
                    {
                        HttpResponseMessage response = await client.GetAsync(uri);
                        if (response.IsSuccessStatusCode)
                        {
                            List<Order> orders = JsonConvert.DeserializeObject<List<Order>>(await response.Content.ReadAsStringAsync());

                            if (orders == null || orders.Count == 0)
                                reachedEnd = true;
                            else if (pageNumber == endingPage)
                            {
                                reachedEnd = true;
                                results.AddRange(orders);
                            }
                            else
                            {
                                results.AddRange(orders);
                            }

                            pageNumber++;
                        }
                        else if (response.ReasonPhrase == "Not Found")
                        {
                            reachedEnd = true;
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                } while (!reachedEnd);

                return results;
            }
        }

        public async Task<List<HistoricalMarketData>> GetMarketStatisticHistory(int regionId, int typeId)
        {
            using (HttpClient client = new HttpClient())
            {
                string url = base.Engine.BaseURL + $"markets/{regionId}/history/?datasource={Engine.DataSource}&type_id={typeId}";
                Uri uri = new Uri(url);
                List<HistoricalMarketData> results = new List<HistoricalMarketData>();

                try
                {
                    HttpResponseMessage response = await client.GetAsync(uri);
                    if (response.IsSuccessStatusCode)
                        results = JsonConvert.DeserializeObject<List<HistoricalMarketData>>(await response.Content.ReadAsStringAsync());
                }
                catch (Exception)
                {

                    throw;
                }

                foreach (HistoricalMarketData result in results)
                {
                    result.RegionId = regionId;
                    result.TypeId = typeId;
                }

                return results;
            }
        }

        public async Task<List<int>> GetRegionItemTypes(int regionId)
        {
            using (HttpClient client = new HttpClient())
            {
                List<int> results = new List<int>();
                int pageNumber = 1;
                int maxPage = 1;
                bool reachedEnd = false;

                do
                {
                    try
                    {
                        string url = base.Engine.BaseURL + $"markets/{regionId}/types/?datasource={Engine.DataSource}&page={pageNumber}";
                        Uri uri = new Uri(url);
                        HttpResponseMessage response = await client.GetAsync(uri);
                        if (response.IsSuccessStatusCode)
                        {
                            results.AddRange(JsonConvert.DeserializeObject<List<int>>(await response.Content.ReadAsStringAsync()));
                            maxPage = 1;//int.Parse(response.Headers.First(x => x.Key == "X-Pages").Value.FirstOrDefault());
                        }
                        else
                        {
                            reachedEnd = true;
                        }

                        if (pageNumber == maxPage)
                            reachedEnd = true;

                        pageNumber++;
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                } while (!reachedEnd);

                return results;
            }
        }
    }
}
