Create table #sellOrders
(
	TypeId int,
	Price decimal(18, 2)
)

Create table #buyOrders
(
	TypeId int,
	Price decimal(18, 2)
)

insert into #sellOrders
select m.TypeId, MIN(m.Price) As Price from marketorder m
where m.IsBuyOrder = 0
group by m.TypeId

insert into #buyOrders
select m.TypeId, MAX(m.PRice) As Price from marketorder m
where m.IsBuyOrder = 1
group by m.TypeId

select d.Description, s.Price As LowestSellOrder, b.Price As HighestBuyOrder, ((b.Price / s.Price) - 1) * - 100 As MarginPercent, s.Price - b.Price As MarginValue, h.Volume
from marketorder m
inner join #sellOrders s on s.TypeId = m.TypeId
inner join #buyOrders b on b.TypeId = m.TypeId
inner join typedescription d on d.Id = m.TypeId
inner join historicalmarketdata h on h.typeId = m.typeid
	and h.Date = '2017-11-15'
group by d.Description, s.PRice, b.Price, h.volume
order by h.Volume desc