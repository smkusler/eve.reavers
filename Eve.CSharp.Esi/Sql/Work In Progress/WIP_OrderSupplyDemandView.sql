select t.Description, 
	t.Id As TypeId,
	m1.RegionId,
	min(m1.Price) As LowestSellOrder, 
	max(m2.price) As HighestBuyOrder, 
	sum(m1.VolumeRemaining) As Supply,
	sum(m2.VolumeRemaining) As Demand,
	Cast(sum(m1.VolumeRemaining) AS FLOAT) / Cast(sum(m2.VolumeRemaining) AS FLOAT) As SupplyDemandEquilibrium,
	min(m1.Price) - max(m2.price) As MarginValue,
	(max(m2.Price) / min(m1.Price) - 1) * -100 As MarginPercent,
	avg(h.Volume) As SevenDayVolumeAverage
from MarketOrder m1
inner join MarketOrder m2 on m1.RegionId = m2.RegionId
	and m1.TypeId = m2.TypeId
inner join TypeDescription t on t.Id = m1.TypeId
inner join HistoricalMarketData h on h.RegionId = m1.RegionId
	and h.TypeId = m1.TypeId
where m1.IsBuyOrder = 0
	and m2.IsBuyOrder = 1
	and h.Date > '2017-11-08'
group by m1.RegionId, t.Description, t.Id
order by SupplyDemandEquilibrium desc
