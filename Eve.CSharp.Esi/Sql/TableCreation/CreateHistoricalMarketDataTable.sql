USE [Eve]
GO

/****** Object:  Table [dbo].[HistoricalMarketData]    Script Date: 11/16/2017 12:57:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HistoricalMarketData](
	[Date] [date] NOT NULL,
	[RegionId] [bigint] NOT NULL,
	[TypeId] [bigint] NOT NULL,
	[Highest] [decimal](18, 2) NOT NULL,
	[Lowest] [decimal](18, 2) NOT NULL,
	[Average] [decimal](18, 2) NOT NULL,
	[OrderCount] [bigint] NOT NULL,
	[Volume] [bigint] NOT NULL,
	[AverageOrderSize]  AS ([Volume]/[OrderCount]),
	[OrdersFilledPerHour]  AS ([OrderCount]/(24)),
	[ItemsSoldPerHour]  AS ([Volume]/(24)),
	[MarginPercent]  AS (([Lowest]/[Highest] - 1)*(-100)),
	[MarginValue]  AS ([Highest]-[Lowest]),
	[DailyCashFlow]  AS ([Volume]*[Average]),
	[MaxIskProfitPerHour]  AS ((([Highest]-[Lowest])*[Volume])/(24))
) ON [PRIMARY]
GO


