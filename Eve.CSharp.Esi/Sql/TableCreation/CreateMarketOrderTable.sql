USE [Eve]
GO

/****** Object:  Table [dbo].[HistoricalMarketData]    Script Date: 11/16/2017 12:57:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MarketOrder](
	[Id] [bigint] NOT NULL,
	[Imported] [date] NOT NULL,
	[RegionId] [bigint] NOT NULL,
	[TypeId] [bigint] NOT NULL,
	[Price] decimal(18, 2) NOT NULL,
	[VolumeRemaining] [bigint] NOT NULL,
	[VolumeTotal] [bigint] NOT NULL,
	[MinVolume] [int] NOT NULL,
	[IsBuyOrder] [bit] NOT NULL,
	[Duration] decimal(18, 4) NOT NULL,
	[Issued] [DateTime] NOT NULL,
	[Range] varchar(50) NOT NULL
) ON [PRIMARY]
GO


