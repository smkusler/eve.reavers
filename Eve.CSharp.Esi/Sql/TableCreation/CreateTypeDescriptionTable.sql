USE [Eve]
GO

/****** Object:  Table [dbo].[TypeDescription]    Script Date: 11/16/2017 4:23:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ItemType](
	[Id] [bigint] NULL,
	[Description] [nvarchar](500) NULL
) ON [PRIMARY]
GO


