﻿using Eve.CSharp.Esi.Controller;
using Eve.CSharp.Esi.Model;
using Eve.CSharp.Esi.Model.Universe;
using Eve.CSharp.Model.Market;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Esi
{
    public class EsiEngine
    {
        public EsiEndpoint Endpoint { get; private set; }
        public EngineDataSource DataSource { get; set; }
        private UniverseController universeController { get; set; }
        private MarketController marketController { get; set; }
        internal readonly string BaseURL;

        // Data source can be adjusted at any time and the application will continue to function.
        public EsiEngine(EsiEndpoint endpoint, EngineDataSource datasource)
        {
            Endpoint = endpoint;
            DataSource = datasource;
            universeController = new UniverseController(this);
            marketController = new MarketController(this);
            BaseURL = $"https://esi.tech.ccp.is/{Endpoint}/";
        }

        #region Universe
        public async Task<Constellation> GetConstellationDetails(int constellationId)
        {
            return await universeController.GetConstellationDetails(constellationId);
        }

        public async Task<List<Constellation>> GetConstellationDetails(List<int> constellationIds)
        {
            return await universeController.GetConstellationDetails(constellationIds);
        }

        public async Task<Region> GetRegionDetails(int regionId)
        {
            return await universeController.GetRegionDetails(regionId);
        }

        public async Task<List<Region>> GetRegionDetails(List<int> regionIds)
        {
            return await universeController.GetRegionDetails(regionIds);
        }

        public async Task<List<int>> GetRegionIds()
        {
            return await universeController.GetRegionIds();
        }

        public async Task<StarSystem> GetSystemDetails(int systemId)
        {
            return await universeController.GetSystemDetails(systemId);
        }

        public async Task<List<StarSystem>> GetSystemDetails(List<int> systemIds)
        {
            return await universeController.GetSystemDetails(systemIds);
        }
        #endregion

        #region Market
        public async Task<List<Order>> GetRegionOrders(int regionId, int typeId = 0, int startingPage = 0, int endingPage = 0)
        {
            return await marketController.GetRegionOrders(regionId, typeId, startingPage, endingPage);
        }

        public async Task<List<Price>> GetPrices()
        {
            return await marketController.GetPrices();
        }

        public async Task<List<int>> GetItemGroups()
        {
            return await marketController.GetItemGroups();
        }

        public async Task<ItemGroup> GetItemGroupDetails(int groupId)
        {
            return await marketController.GetItemGroupDetails(groupId);
        }

        public async Task<List<HistoricalMarketData>> GetMarketStatisticHistory(int regionId, int typeId)
        {
            return await marketController.GetMarketStatisticHistory(regionId, typeId);
        }

        public async Task<List<int>> GetRegionItemTypes(int regionId)
        {
            return await marketController.GetRegionItemTypes(regionId);
        }
        #endregion
    }

    // Tranquility is production.
    // Serentiy is test.
    // Both must be lowercase in order to be used as a part of the URI.  URI is case sensitive.
    public enum EngineDataSource
    {
        tranquility,
        singularity
    }

    // Esi supports up to 3 endpoints.  Production is latest.  Dev and Legacy are both test or deprecated endpoints.
    // All must be lowercase as they are used to dynamically generate URI and URI are case sensitive.
    public enum EsiEndpoint
    {
        dev,
        latest,
        legacy
    }
}
