﻿using Dapper;
using Eve.CSharp.Model.Market;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Repository
{
    public class MarketRepository
    {
        private string _connectionString { get; set; }

        public MarketRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int InsertHistoricalMarketStatistics(List<HistoricalMarketData> stats)
        {
            //const string sql = @"
            //    INSERT INTO HistoricalMarketStatistics (Date, RegionId, TypeId, Highest, Lowest, Average, OrderCount, AverageOrderSize,
            //    OrdersFilledPerHour, Volume, ItemsSoldPerHour, MarginPercent, MarginValueInIsk, DailyCashFlow, MaxIskPerHour)
            //    VALUES (@TypeId, @RegionId, @Date, @Highest, @Lowest, @Average, @Volume, @OrderCount, 
            //        @AverageOrderSize, @MarginPercent, @MarginValueInIsk, @DailyCashFlow, @MaxIskPerHour)";

            const string sql = @"
                INSERT INTO HistoricalMarketData (Date, RegionId, TypeId, Highest, Lowest, Average, OrderCount, Volume)
                VALUES (@Date, @RegionId, @TypeId, @Highest, @Lowest, @Average, @OrderCount, @Volume)";
            int rowsAffected = 0;
            int position = 1;
            HistoricalMarketData currentStat = new HistoricalMarketData();

            try
            {
                using (IDbConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    foreach (HistoricalMarketData stat in stats)
                    {
                        currentStat = stat;

                        rowsAffected += connection.Execute(sql,
                            new
                            {
                                Date = stat.Date,
                                RegionId = stat.RegionId,
                                TypeId = stat.TypeId,
                                Highest = stat.Highest,
                                Lowest = stat.Lowest,
                                Average = stat.Average,
                                OrderCount = stat.OrderCount,
                                Volume = stat.Volume,
                            });

                        position++;
                    }

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {
                var pos = position;
                var curr = currentStat;
                throw ex;
            }
        }

        public int InsertTypeIds()
        {
            List<string> lines = new List<string>();

            try
            {
                using (StreamReader reader = new StreamReader(new FileStream("typeId.txt", FileMode.Open)))
                {
                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            List<ItemType> types = new List<ItemType>();

            foreach (string line in lines)
            {
                if (!string.IsNullOrEmpty(line))
                {
                    string[] lineParts = line.Split(' ');
                    string id = lineParts[0];
                    StringBuilder builder = new StringBuilder();

                    for (int i = 1; i < lineParts.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(lineParts[i]))
                            builder.Append(lineParts[i] + ' ');
                    }

                    types.Add(new ItemType() { Id = Convert.ToInt64(id), Description = builder.ToString().Trim() });
                }
            }

            const string sql = @"
                INSERT INTO ItemType (Id, Description)
                VALUES(@Id, @Description)";
            int rowsAffected = 0;

            try
            {
                using (IDbConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    foreach (ItemType type in types)
                    {
                        rowsAffected += connection.Execute(sql, new { Id = type.Id, Description = type.Description });
                    }

                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public int InsertMarketOrders(List<Order> orders)
        {
            const string sql = @"
                INSERT INTO MarketOrder (Id, Imported, RegionId, TypeId, Price, VolumeRemaining,
                    VolumeTotal, MinVolume, IsBuyOrder, Duration, Issued, Range)
                VALUES (@Id, @Imported, @RegionId, @TypeId, @Price, @VolumeRemaining,
                    @VolumeTotal, @MinVolume, @IsBuyOrder, @Duration, @Issued, @Range)";
            int affectedRows = 0;

            try
            {
                using (IDbConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    foreach (Order order in orders)
                    {
                        affectedRows += connection.Execute(sql, new {
                            Id = order.Id,
                            Imported = DateTime.Now,
                            RegionId = order.RegionId,
                            TypeId = order.TypeId,
                            Price = order.Price,
                            VolumeRemaining = order.VolumeRemaining,
                            VolumeTotal = order.VolumeTotal,
                            MinVolume = order.MinVolume,
                            IsBuyOrder = order.IsBuyOrder,
                            Duration = order.Duration,
                            Issued = order.Issued,
                            Range = order.Range
                        });
                    }

                    return affectedRows;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
