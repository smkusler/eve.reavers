﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Model.Market
{
    public class HistoricalMarketData
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }
        [JsonProperty("order_count")]
        public int OrderCount { get; set; }
        [JsonProperty("volume")]
        public long Volume { get; set; }
        [JsonProperty("highest")]
        public decimal Highest { get; set; }
        [JsonProperty("average")]
        public decimal Average { get; set; }
        [JsonProperty("lowest")]
        public decimal Lowest { get; set; }
        public long TypeId { get; set; }
        public long RegionId { get; set; }
        public decimal MarginPercent => decimal.Round((Lowest / Highest - 1) * -100, 2);
        public decimal MarginValue => decimal.Round(Highest - Lowest, 2);
        public decimal AverageOrderSize => decimal.Round(Volume / OrderCount, 2);
        public decimal DailyCashFlow => decimal.Round(Volume * Average, 2);
        public decimal MaxIskProfitPerHour => decimal.Round((MarginValue * Volume) / 24, 2);
        public decimal ItemsSoldPerHour => decimal.Round((Volume) / 24, 2);
        public decimal OrdersFilledPerHour => decimal.Round((OrderCount) / 24, 2);

        public override string ToString()
        {
            return $"{Date},{RegionId},{TypeId},{OrderCount},{Volume},{Highest},{Average},{Lowest},{MarginPercent}" +
                $",{MarginValue},{AverageOrderSize},{DailyCashFlow},{MaxIskProfitPerHour},{ItemsSoldPerHour},{OrdersFilledPerHour}";
        }
    }
}
