﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Model.Market
{
    public class Order
    {
        [JsonProperty("order_id")]
        public long Id { get; set; }
        [JsonProperty("type_id")]
        public long TypeId { get; set; }
        [JsonProperty("location_id")]
        public long LocationId { get; set; }
        [JsonProperty("volume_total")]
        public long VolumeTotal { get; set; }
        [JsonProperty("volume_remain")]
        public long VolumeRemaining { get; set; }
        [JsonProperty("min_volume")]
        public int MinVolume { get; set; }
        [JsonProperty("price")]
        public double Price { get; set; }
        [JsonProperty("is_buy_order")]
        public bool IsBuyOrder { get; set; }
        [JsonProperty("duration")]
        public decimal Duration { get; set; }
        [JsonProperty("issued")]
        public DateTime Issued { get; set; }
        [JsonProperty("range")]
        public string Range { get; set; }
        public int RegionId { get; set; }
    }
}
