﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Model.Market
{
    public class ItemGroup
    {
        [JsonProperty("market_group_id")]
        public long Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("types")]
        public int[] TypeIds { get; set; }
        [JsonProperty("parent_group_id")]
        public long ParentGroupId { get; set; }
    }
}
