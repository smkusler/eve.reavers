﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Model.Market
{
    class ItemType
    {
		public long Id { get; set; }
		public string Description { get; set; }
    }
}
