﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Model.Market
{
    public class Price
    {
        [JsonProperty("type_id")]
        public int TypeId { get; set; }
        [JsonProperty("average_price")]
        public decimal AveragePrice { get; set; }
        [JsonProperty("adjusted_price")]
        public decimal AdjustedPrice { get; set; }
    }
}
