﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Model.TradeRecommendation
{
    public class TradeRecommendation
    {
        public int BuyRegionId { get; set; }
        public int SellRegionId { get; set; }
        public int TypeId { get; set; }
    }
}
