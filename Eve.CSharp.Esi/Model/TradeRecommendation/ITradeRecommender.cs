﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Model.TradeRecommendation
{
    public interface ITradeRecommender
    {
        TradeRecommendation Recommend();
    }
}
