﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Model.TradeRecommendation
{
    public static class TradeRecommendationHelper
    {
        public static double GenerateValue(double volumePercentile, double marginPercent, double supplyDemandRatio)
        {
            double result = Math.Pow(volumePercentile, 2.3) * (Math.Sqrt(marginPercent));
            result /= Math.Sqrt(supplyDemandRatio);

            return result;
        }
    }
}
