﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Esi.Model.Universe
{
    public class StarSystem
    {
        [JsonProperty("system_id")]
        public int SystemId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("position")]
        public Position Position { get; set; }
        [JsonProperty("security_status")]
        public double SecurityStatus { get; set; }
        [JsonProperty("constellation_id")]
        public int ConstellationId { get; set; }
        [JsonProperty("planets")]
        public PlanetMoonPairing[] PlanetMoonPairings { get; set; }
        [JsonProperty("stargates")]
        public int[] StargateIds { get; set; }
        [JsonProperty("star_id")]
        public int StarId { get; set; }
        [JsonProperty("security_class")]
        public char SecurityClass { get; set; }
    }
}
