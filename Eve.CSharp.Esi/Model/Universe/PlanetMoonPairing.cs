﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Esi.Model.Universe
{
    public class PlanetMoonPairing
    {
        [JsonProperty("planet_id")]
        public int Planet_Id { get; set; }
        [JsonProperty("moons")]
        public int[] MoonIds { get; set; }
    }
}
