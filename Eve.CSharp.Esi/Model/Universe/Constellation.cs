﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Esi.Model.Universe
{
    public class Constellation
    {
        [JsonProperty("constellation_id")]
        public int ConstellationId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("position")]
        public Position Position { get; set; }
        [JsonProperty("region_id")]
        public int RegionId { get; set; }
        [JsonProperty("systems")]
        public int[] SystemIds { get; set; }
    }
}
