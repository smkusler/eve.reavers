﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.CSharp.Esi.Model.Universe
{
    public class Planet
    {
        [JsonProperty("planet_id")]
        public int PlanetId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("type_id")]
        public int TypeId { get; set; }
        [JsonProperty("position")]
        public Position Position { get; set; }
        [JsonProperty("system_id")]
        public int SystemId { get; set; }
    }
}
