﻿using Eve.CSharp.Esi;
using Eve.CSharp.Model.Market;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Eve.Reavers.Refinery
{
    public partial class RefineryCalculator : Form
    {
        private EsiEngine engine;
        private const int JITA = 10000002;

        private const int NANOTRANSISTORS = 16681;
        private const int ATMOSPHERIC_GAS = 16634;
        private const int EVAPORITE_DEPOSITS = 16635;
        private const int PLATINUM = 16644;
        private const int TECHNETIUM = 16649;
        private const int NITROGEN_ISOTOPES = 17888;
        private const int MERCURY = 16646;
        private const int NEODYMIUM = 16651;
        private const int HELIUM_ISOTOPES = 16274;

        private double _nanotransistorPrice;
        private double _atmostphericGasPrice;
        private double _evaporiteDepositPrice;
        private double _platinumPrice;
        private double _technetiumPrice;
        private double _nitrogenIsotopePrice;
        private double _mercuryPrice;
        private double _neodymiumPrice;
        private double _heliumIsotopePrice;

        private int _numberOfSimpleRefineries = 0;
        private int _numberOfComplexRefineries = 0;

        public RefineryCalculator()
        {
            InitializeComponent();
            engine = new EsiEngine(EsiEndpoint.latest, EngineDataSource.tranquility);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SetPrices().Wait();
        }

        private void txtNumberOfSimpleRefineries_TextChanged(object sender, EventArgs e)
        {
            if (IsNumericAndGreaterThanZero(txtNumberOfSimpleRefineries.Text))
            {
                UpdateUi();
            }
            else
            {
                txtNumberOfSimpleRefineries.Text = _numberOfSimpleRefineries.ToString();
            }
        }

        private void txtNumberOfComplexRefineries_TextChanged(object sender, EventArgs e)
        {
            if (IsNumericAndGreaterThanZero(txtNumberOfComplexRefineries.Text))
            {
                UpdateUi();
            }
            else
            {
                txtNumberOfComplexRefineries.Text = _numberOfComplexRefineries.ToString();
                MessageBox.Show("Number of refineries must be entered as a number > -1 and be a whole number.", "CANT HAVE PARTIAL REFINERIES HOMIE");
            }
        }

        public void UpdateUi()
        {
            List<ReactorInput> inputsToDisplay = new List<ReactorInput>();
            inputsToDisplay.Add(new ReactorInput() { Mineral = "Nanotransistor", DailyCost = _nanotransistorPrice, DailyInput = _numberOfSimpleRefineries, ReactorType = ReactorType.Simple });
            inputsToDisplay.Add(new ReactorInput() { Mineral = "Atmospheric Gas", DailyCost = _atmostphericGasPrice, DailyInput = _numberOfSimpleRefineries, ReactorType = ReactorType.Simple });
            inputsToDisplay.Add(new ReactorInput() { Mineral = "Evaporite Deposits", DailyCost = _evaporiteDepositPrice, DailyInput = _numberOfSimpleRefineries, ReactorType = ReactorType.Simple });
            inputsToDisplay.Add(new ReactorInput() { Mineral = "Platinum", DailyCost = _platinumPrice, DailyInput = _numberOfSimpleRefineries, ReactorType = ReactorType.Simple });
            inputsToDisplay.Add(new ReactorInput() { Mineral = "Technetium", DailyCost = _technetiumPrice, DailyInput = _numberOfSimpleRefineries, ReactorType = ReactorType.Simple });
            inputsToDisplay.Add(new ReactorInput() { Mineral = "Nitrogen Isotopes", DailyCost = _nitrogenIsotopePrice, DailyInput = _numberOfComplexRefineries, ReactorType = ReactorType.Complex });
            inputsToDisplay.Add(new ReactorInput() { Mineral = "Mercury", DailyCost = _mercuryPrice, DailyInput = _numberOfSimpleRefineries, ReactorType = ReactorType.Simple });
            inputsToDisplay.Add(new ReactorInput() { Mineral = "Neodymium", DailyCost = _neodymiumPrice, DailyInput = _numberOfSimpleRefineries, ReactorType = ReactorType.Simple });
            inputsToDisplay.Add(new ReactorInput() { Mineral = "HeliumIsotopes", DailyCost = HELIUM_ISOTOPES, DailyInput = _numberOfSimpleRefineries, ReactorType = ReactorType.Simple });

            dgSimpleReactorInput.DataSource = inputsToDisplay.Where(x => x.ReactorType == ReactorType.Simple).ToList();
            dgComplexReactorInput.DataSource = inputsToDisplay.Where(x => x.ReactorType == ReactorType.Complex).ToList();
        }

        public async Task SetPrices()
        {
            List<Order> orders = await engine.GetRegionOrders(JITA, NANOTRANSISTORS);
            _nanotransistorPrice = orders.Where(x => x.IsBuyOrder == false).Min(x => x.Price);
            orders = await engine.GetRegionOrders(JITA, ATMOSPHERIC_GAS);
            _atmostphericGasPrice = orders.Where(x => x.IsBuyOrder == false).Min(x => x.Price);
            orders = await engine.GetRegionOrders(JITA, EVAPORITE_DEPOSITS);
            _evaporiteDepositPrice = orders.Where(x => x.IsBuyOrder == false).Min(x => x.Price);
            orders = await engine.GetRegionOrders(JITA, PLATINUM);
            _platinumPrice = orders.Where(x => x.IsBuyOrder == false).Min(x => x.Price);
            orders = await engine.GetRegionOrders(JITA, TECHNETIUM);
            _technetiumPrice = orders.Where(x => x.IsBuyOrder == false).Min(x => x.Price);
            orders = await engine.GetRegionOrders(JITA, NITROGEN_ISOTOPES);
            _nitrogenIsotopePrice = orders.Where(x => x.IsBuyOrder == false).Min(x => x.Price);
            orders = await engine.GetRegionOrders(JITA, MERCURY);
            _mercuryPrice = orders.Where(x => x.IsBuyOrder == false).Min(x => x.Price);
            orders = await engine.GetRegionOrders(JITA, NEODYMIUM);
            _neodymiumPrice = orders.Where(x => x.IsBuyOrder == false).Min(x => x.Price);
            orders = await engine.GetRegionOrders(JITA, HELIUM_ISOTOPES);
            _heliumIsotopePrice = orders.Where(x => x.IsBuyOrder == false).Min(x => x.Price);
        }

        public bool IsNumericAndGreaterThanZero(string text)
        {
            double value;
            double.TryParse(text, out value);

            if (value <= 0 && !(value % 1 == 0))
                return false;

            return true;
        }
    }
}
