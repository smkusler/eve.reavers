﻿namespace Eve.Reavers.Refinery
{
    partial class RefineryCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNumberOfComplexRefineries = new System.Windows.Forms.TextBox();
            this.txtNumberOfSimpleRefineries = new System.Windows.Forms.TextBox();
            this.dgComplexReactorInput = new System.Windows.Forms.DataGridView();
            this.mineralDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dailyInputDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dailyCostDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monthlyInputDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monthlyCostDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reactorInputBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgSimpleReactorInput = new System.Windows.Forms.DataGridView();
            this.mineralDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dailyInputDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dailyCostDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monthlyInputDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monthlyCostDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTotalDailyCost = new System.Windows.Forms.TextBox();
            this.txtMonthlyProduction = new System.Windows.Forms.TextBox();
            this.txtMonthlyCost = new System.Windows.Forms.TextBox();
            this.txtMonthlyRevenue = new System.Windows.Forms.TextBox();
            this.txtMonthlyProfit = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgComplexReactorInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reactorInputBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSimpleReactorInput)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 402);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(354, 86);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "Always group refineries in a group of 5. Use 3 refineries to produce simple react" +
    "ions that are then used to feed the other 2 advanced reaction refineries.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 386);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "NOTES";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 281);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "HOW TO USE";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(12, 297);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(354, 86);
            this.richTextBox2.TabIndex = 3;
            this.richTextBox2.Text = "Update price index on page A2 appropriately, then simply adjust the amount of Sim" +
    "ple / Complex refineries are being used and the rest is calculated automatically" +
    ".";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(533, 443);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "# of Simple Athanors";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(524, 470);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "# of Complex Athanors";
            // 
            // txtNumberOfComplexRefineries
            // 
            this.txtNumberOfComplexRefineries.Location = new System.Drawing.Point(644, 468);
            this.txtNumberOfComplexRefineries.Name = "txtNumberOfComplexRefineries";
            this.txtNumberOfComplexRefineries.Size = new System.Drawing.Size(100, 20);
            this.txtNumberOfComplexRefineries.TabIndex = 6;
            this.txtNumberOfComplexRefineries.Text = "0";
            this.txtNumberOfComplexRefineries.TextChanged += new System.EventHandler(this.txtNumberOfComplexRefineries_TextChanged);
            // 
            // txtNumberOfSimpleRefineries
            // 
            this.txtNumberOfSimpleRefineries.Location = new System.Drawing.Point(644, 440);
            this.txtNumberOfSimpleRefineries.Name = "txtNumberOfSimpleRefineries";
            this.txtNumberOfSimpleRefineries.Size = new System.Drawing.Size(100, 20);
            this.txtNumberOfSimpleRefineries.TabIndex = 7;
            this.txtNumberOfSimpleRefineries.Text = "0";
            this.txtNumberOfSimpleRefineries.TextChanged += new System.EventHandler(this.txtNumberOfSimpleRefineries_TextChanged);
            // 
            // dgComplexReactorInput
            // 
            this.dgComplexReactorInput.AllowUserToAddRows = false;
            this.dgComplexReactorInput.AllowUserToDeleteRows = false;
            this.dgComplexReactorInput.AutoGenerateColumns = false;
            this.dgComplexReactorInput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgComplexReactorInput.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mineralDataGridViewTextBoxColumn,
            this.dailyInputDataGridViewTextBoxColumn,
            this.dailyCostDataGridViewTextBoxColumn,
            this.monthlyInputDataGridViewTextBoxColumn,
            this.monthlyCostDataGridViewTextBoxColumn});
            this.dgComplexReactorInput.DataSource = this.reactorInputBindingSource;
            this.dgComplexReactorInput.Location = new System.Drawing.Point(12, 13);
            this.dgComplexReactorInput.Name = "dgComplexReactorInput";
            this.dgComplexReactorInput.ReadOnly = true;
            this.dgComplexReactorInput.Size = new System.Drawing.Size(732, 91);
            this.dgComplexReactorInput.TabIndex = 8;
            // 
            // mineralDataGridViewTextBoxColumn
            // 
            this.mineralDataGridViewTextBoxColumn.DataPropertyName = "Mineral";
            this.mineralDataGridViewTextBoxColumn.HeaderText = "Mineral";
            this.mineralDataGridViewTextBoxColumn.Name = "mineralDataGridViewTextBoxColumn";
            this.mineralDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dailyInputDataGridViewTextBoxColumn
            // 
            this.dailyInputDataGridViewTextBoxColumn.DataPropertyName = "DailyInput";
            this.dailyInputDataGridViewTextBoxColumn.HeaderText = "DailyInput";
            this.dailyInputDataGridViewTextBoxColumn.Name = "dailyInputDataGridViewTextBoxColumn";
            this.dailyInputDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dailyCostDataGridViewTextBoxColumn
            // 
            this.dailyCostDataGridViewTextBoxColumn.DataPropertyName = "DailyCost";
            this.dailyCostDataGridViewTextBoxColumn.HeaderText = "DailyCost";
            this.dailyCostDataGridViewTextBoxColumn.Name = "dailyCostDataGridViewTextBoxColumn";
            this.dailyCostDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // monthlyInputDataGridViewTextBoxColumn
            // 
            this.monthlyInputDataGridViewTextBoxColumn.DataPropertyName = "MonthlyInput";
            this.monthlyInputDataGridViewTextBoxColumn.HeaderText = "MonthlyInput";
            this.monthlyInputDataGridViewTextBoxColumn.Name = "monthlyInputDataGridViewTextBoxColumn";
            this.monthlyInputDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // monthlyCostDataGridViewTextBoxColumn
            // 
            this.monthlyCostDataGridViewTextBoxColumn.DataPropertyName = "MonthlyCost";
            this.monthlyCostDataGridViewTextBoxColumn.HeaderText = "MonthlyCost";
            this.monthlyCostDataGridViewTextBoxColumn.Name = "monthlyCostDataGridViewTextBoxColumn";
            this.monthlyCostDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reactorInputBindingSource
            // 
            this.reactorInputBindingSource.DataSource = typeof(Eve.Reavers.Refinery.ReactorInput);
            // 
            // dgSimpleReactorInput
            // 
            this.dgSimpleReactorInput.AllowUserToAddRows = false;
            this.dgSimpleReactorInput.AllowUserToDeleteRows = false;
            this.dgSimpleReactorInput.AutoGenerateColumns = false;
            this.dgSimpleReactorInput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSimpleReactorInput.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mineralDataGridViewTextBoxColumn1,
            this.dailyInputDataGridViewTextBoxColumn1,
            this.dailyCostDataGridViewTextBoxColumn1,
            this.monthlyInputDataGridViewTextBoxColumn1,
            this.monthlyCostDataGridViewTextBoxColumn1});
            this.dgSimpleReactorInput.DataSource = this.reactorInputBindingSource;
            this.dgSimpleReactorInput.Location = new System.Drawing.Point(12, 111);
            this.dgSimpleReactorInput.Name = "dgSimpleReactorInput";
            this.dgSimpleReactorInput.ReadOnly = true;
            this.dgSimpleReactorInput.Size = new System.Drawing.Size(732, 167);
            this.dgSimpleReactorInput.TabIndex = 9;
            // 
            // mineralDataGridViewTextBoxColumn1
            // 
            this.mineralDataGridViewTextBoxColumn1.DataPropertyName = "Mineral";
            this.mineralDataGridViewTextBoxColumn1.HeaderText = "Mineral";
            this.mineralDataGridViewTextBoxColumn1.Name = "mineralDataGridViewTextBoxColumn1";
            this.mineralDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dailyInputDataGridViewTextBoxColumn1
            // 
            this.dailyInputDataGridViewTextBoxColumn1.DataPropertyName = "DailyInput";
            this.dailyInputDataGridViewTextBoxColumn1.HeaderText = "DailyInput";
            this.dailyInputDataGridViewTextBoxColumn1.Name = "dailyInputDataGridViewTextBoxColumn1";
            this.dailyInputDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dailyCostDataGridViewTextBoxColumn1
            // 
            this.dailyCostDataGridViewTextBoxColumn1.DataPropertyName = "DailyCost";
            this.dailyCostDataGridViewTextBoxColumn1.HeaderText = "DailyCost";
            this.dailyCostDataGridViewTextBoxColumn1.Name = "dailyCostDataGridViewTextBoxColumn1";
            this.dailyCostDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // monthlyInputDataGridViewTextBoxColumn1
            // 
            this.monthlyInputDataGridViewTextBoxColumn1.DataPropertyName = "MonthlyInput";
            this.monthlyInputDataGridViewTextBoxColumn1.HeaderText = "MonthlyInput";
            this.monthlyInputDataGridViewTextBoxColumn1.Name = "monthlyInputDataGridViewTextBoxColumn1";
            this.monthlyInputDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // monthlyCostDataGridViewTextBoxColumn1
            // 
            this.monthlyCostDataGridViewTextBoxColumn1.DataPropertyName = "MonthlyCost";
            this.monthlyCostDataGridViewTextBoxColumn1.HeaderText = "MonthlyCost";
            this.monthlyCostDataGridViewTextBoxColumn1.Name = "monthlyCostDataGridViewTextBoxColumn1";
            this.monthlyCostDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // txtTotalDailyCost
            // 
            this.txtTotalDailyCost.Location = new System.Drawing.Point(411, 313);
            this.txtTotalDailyCost.Name = "txtTotalDailyCost";
            this.txtTotalDailyCost.ReadOnly = true;
            this.txtTotalDailyCost.Size = new System.Drawing.Size(150, 20);
            this.txtTotalDailyCost.TabIndex = 10;
            // 
            // txtMonthlyProduction
            // 
            this.txtMonthlyProduction.Location = new System.Drawing.Point(411, 356);
            this.txtMonthlyProduction.Name = "txtMonthlyProduction";
            this.txtMonthlyProduction.ReadOnly = true;
            this.txtMonthlyProduction.Size = new System.Drawing.Size(150, 20);
            this.txtMonthlyProduction.TabIndex = 11;
            // 
            // txtMonthlyCost
            // 
            this.txtMonthlyCost.Location = new System.Drawing.Point(570, 313);
            this.txtMonthlyCost.Name = "txtMonthlyCost";
            this.txtMonthlyCost.ReadOnly = true;
            this.txtMonthlyCost.Size = new System.Drawing.Size(171, 20);
            this.txtMonthlyCost.TabIndex = 12;
            // 
            // txtMonthlyRevenue
            // 
            this.txtMonthlyRevenue.Location = new System.Drawing.Point(570, 356);
            this.txtMonthlyRevenue.Name = "txtMonthlyRevenue";
            this.txtMonthlyRevenue.ReadOnly = true;
            this.txtMonthlyRevenue.Size = new System.Drawing.Size(171, 20);
            this.txtMonthlyRevenue.TabIndex = 13;
            // 
            // txtMonthlyProfit
            // 
            this.txtMonthlyProfit.Location = new System.Drawing.Point(570, 399);
            this.txtMonthlyProfit.Name = "txtMonthlyProfit";
            this.txtMonthlyProfit.ReadOnly = true;
            this.txtMonthlyProfit.Size = new System.Drawing.Size(171, 20);
            this.txtMonthlyProfit.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(408, 297);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "TOTAL DAILY COST";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(567, 297);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "TOTAL MONTHLY COST";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(408, 340);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(153, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "MONTHLY PRODUCTION";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(567, 340);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(174, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "TOTAL MONTHLY REVENUE";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(567, 383);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "TOTAL MONTHLY PROFIT";
            // 
            // RefineryCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 498);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtMonthlyProfit);
            this.Controls.Add(this.txtMonthlyRevenue);
            this.Controls.Add(this.txtMonthlyCost);
            this.Controls.Add(this.txtMonthlyProduction);
            this.Controls.Add(this.txtTotalDailyCost);
            this.Controls.Add(this.dgSimpleReactorInput);
            this.Controls.Add(this.dgComplexReactorInput);
            this.Controls.Add(this.txtNumberOfSimpleRefineries);
            this.Controls.Add(this.txtNumberOfComplexRefineries);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBox1);
            this.Name = "RefineryCalculator";
            this.Text = "Reavers Refinery Calculator";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgComplexReactorInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reactorInputBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSimpleReactorInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNumberOfComplexRefineries;
        private System.Windows.Forms.TextBox txtNumberOfSimpleRefineries;
        private System.Windows.Forms.DataGridView dgComplexReactorInput;
        private System.Windows.Forms.DataGridView dgSimpleReactorInput;
        private System.Windows.Forms.DataGridViewTextBoxColumn mineralDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dailyInputDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dailyCostDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn monthlyInputDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn monthlyCostDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource reactorInputBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn mineralDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dailyInputDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dailyCostDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn monthlyInputDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn monthlyCostDataGridViewTextBoxColumn1;
        private System.Windows.Forms.TextBox txtTotalDailyCost;
        private System.Windows.Forms.TextBox txtMonthlyProduction;
        private System.Windows.Forms.TextBox txtMonthlyCost;
        private System.Windows.Forms.TextBox txtMonthlyRevenue;
        private System.Windows.Forms.TextBox txtMonthlyProfit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}

