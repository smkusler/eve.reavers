﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eve.Reavers.Refinery
{
    public enum ReactorType
    {
        Simple,
        Complex
    }

    public class ReactorInput
    {
        public string Mineral { get; set; }
        public double DailyInput { get; set; }
        public double DailyCost { get; set; }
        public ReactorType ReactorType { get; set; }
        public double MonthlyInput => DailyInput * 30;
        public double MonthlyCost => DailyCost * 30;
    }
}
